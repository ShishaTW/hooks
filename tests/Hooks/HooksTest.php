<?php

namespace Test\Hooks;

use PHPUnit\Framework\TestCase;
use Hooks\Hooks;
use Hooks\HookManager;
use Test\Demo\ServiceProvider\SayHelloWorldServiceProvider;

class HooksTest extends TestCase
{
    public function testSingleton()
    {
        $instance = Hooks::getInstance();
        
        $this->assertInstanceOf(HookManager::class, $instance);
        
        $instance = Hooks::setInstance(new HookManager);
        
        $this->assertSame($instance, Hooks::getInstance());
    }
    
    public function testMagicMethod()
    {
        Hooks::addHook(SayHelloWorldServiceProvider::class);
        
        $this->assertEquals('Hello world', Hooks::make('say'));
    }
}
