<?php

namespace Test\Hooks;

use PHPUnit\Framework\TestCase;

use Hooks\Hooks;
use Test\Demo\Cat\CatFacade;
use Test\Demo\Cat\CatServiceProvider;

class FacadeTest extends TestCase
{
    public function testFacade()
    {
        // 註冊服務
        Hooks::addHook(CatServiceProvider::class);

        $this->assertEquals('meow', CatFacade::say());
    }
}
