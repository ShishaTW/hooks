<?php

namespace Test\Hooks;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Test\Demo\Singleton;

class SingletonTest extends TestCase
{
    public function testSingleton()
    {
        $instance = Singleton::getInstance();
        
        $this->assertInstanceOf(Singleton::class, $instance);
        
        $instance = Singleton::setInstance(new Singleton);
        
        $this->assertSame($instance, Singleton::getInstance());
    }
    
    public function testException()
    {
        $this->expectException(InvalidArgumentException::class);
        Singleton::setInstance(null);
    }
    
    public function testMagicMethod()
    {
        $this->assertEquals('Hello world', Singleton::echo('Hello world'));
    }
}
