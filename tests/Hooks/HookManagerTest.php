<?php

namespace Test\Hooks;

use PHPUnit\Framework\TestCase;
use Hooks\HookManager;
use Hooks\ServiceProvider;
use Test\Demo\ServiceProvider\TimerServiceProvider;
use Test\Demo\ServiceProvider\GetConfigServiceProvider;
use Test\Demo\ServiceProvider\SayHelloWorldServiceProvider;

class HookManagerTest extends TestCase
{
    public $hooks;
    
    public function setUp()
    {
        $this->hooks = new HookManager;
    }
    
    public function testRegister()
    {
        $this->hooks->addHook(SayHelloWorldServiceProvider::class);
        
        $this->assertEquals('Hello world', $this->hooks->make('say'));
    }
    
    public function testTimerServiceProvider()
    {
        $this->hooks->addHook(TimerServiceProvider::class);
        
        $this->assertEquals(0, $this->hooks->make('get'));
        $this->hooks->make('add');
        $this->assertEquals(1, $this->hooks->make('get'));
    }
    
    public function testRegisterWithConfig()
    {
        $configFile = __DIR__ . '/../config/demo.php';
        $this->hooks->addHook(GetConfigServiceProvider::class, $configFile);
        
        $this->assertEquals(require $configFile, $this->hooks->make('getConfig')['demo']);
    }
    
    public function testBooted()
    {
        // 先初始化
        $this->hooks->boot();
        
        $provider = $this->createMock(ServiceProvider::class);
        $provider->expects($this->once())->method('register');
        $provider->expects($this->once())->method('boot');
        
        $this->hooks->addHook($provider);
        
        // 不做任何事
        $this->hooks->boot();
    }
}
