<?php

namespace Test\Demo\Cat;

use Hooks\ServiceProvider;

class CatServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Cat::class, function ($app) {
            return new Cat;
        });
    }

    public function boot()
    {
        //
    }
}
