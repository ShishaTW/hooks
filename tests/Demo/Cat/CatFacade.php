<?php

namespace Test\Demo\Cat;

use Hooks\Facades\Facade;

class CatFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Cat::class;
    }
}
