<?php

namespace Test\Demo\ServiceProvider;

use Hooks\ServiceProvider;

class GetConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('getConfig', function ($app) {
            return $app['config'];
        });
    }
    
    public function boot()
    {
        //
    }
}
