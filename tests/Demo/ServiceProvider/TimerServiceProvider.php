<?php

namespace Test\Demo\ServiceProvider;

use Hooks\ServiceProvider;

class TimerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $app['config']['count'] = 0;
    }
    
    public function boot()
    {
        $this->app->bind('get', function ($app) {
            return $app['config']['count'];
        });
        
        $this->app->bind('add', function ($app) {
            return $app['config']['count'] += 1;
        });
    }
}
