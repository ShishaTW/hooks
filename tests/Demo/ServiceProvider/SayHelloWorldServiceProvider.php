<?php

namespace Test\Demo\ServiceProvider;

use Hooks\ServiceProvider;

class SayHelloWorldServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('say', function ($app) {
            return 'Hello world';
        });
    }
    
    public function boot()
    {
        //
    }
}
