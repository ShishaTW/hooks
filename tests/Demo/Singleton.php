<?php

namespace Test\Demo;

use Hooks\Singleton as BaseSingleton;
use Hooks\SingletonAccessor;

class Singleton
{
    use BaseSingleton, SingletonAccessor;
    
    public function echo($value)
    {
        return $value;
    }
}
