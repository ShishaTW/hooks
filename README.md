# Hooks

Hooks 是一個獨立運行的服務容器，包裝 Laravel framework 的 Illuminate\Container。
方便在非 Laravel 環境或傳統 PHP 專案底下，依然可使用依賴注入方式生成物件。


## Installation

使用 composer 安裝

```bash
composer config repositories.hooks vcs https://ShishaTW@bitbucket.org/ShishaTW/hooks.git
composer require "shishamou/hooks:0.1.*"
```

## Usage

### 生成類別實例
使用 `make` 方法解析並生成類別實例。詳細請參考 `https://docs.laravel-dojo.com/laravel/5.5/container#the-make-method`

```php
use Hooks\Hooks;

$repository = Hooks::make(MyProject\Repository::class);
```

### 註冊服務

```php
use Hooks\Hooks;

Hooks::addHook(
    MyProject\RepositoryServiceProvider::class,
    __DIR__ . '/../config/repository.php'
);
```