<?php
/**
 * Facade
 *
 * @author Shisha <shisha225@gmail.com>
 * @since 2016-10-04
 */

namespace Hooks\Facades;

use Hooks\Hooks;
use RuntimeException;

abstract class Facade
{
    /**
     * @var array
     */
    protected static $resolvedInstance;

    /**
     * 註冊 facade
     */
    abstract protected static function getFacadeAccessor();

    /**
     * 靜態呼叫物件實例
     *
     * @param string
     * @param array
     * @return mixed
     * @throws \RuntimeException
     */
    public static function __callStatic($method, $arguments)
    {
        if ($instance = self::getFacadeInstance()) {
            switch (count($arguments)) {
                case 0:
                    return $instance->$method();

                case 1:
                    return $instance->$method($arguments[0]);

                case 2:
                    return $instance->$method($arguments[0], $arguments[1]);

                case 3:
                    return $instance->$method($arguments[0], $arguments[1], $arguments[2]);

                default:
                    return call_user_func_array([$instance, $method], $arguments);
            }
        }

        throw new RuntimeException('Facade 未指定。');
    }

    /**
     * 取得實例
     *
     * @return mixed
     */
    public static function getFacadeInstance()
    {
        return self::resolveFacadeInstance(static::getFacadeAccessor());
    }

    /**
     * 解析 facade instance, 若有則回傳, 反之進行建構
     *
     * @param  string|object
     * @return mixed
     */
    protected static function resolveFacadeInstance($name)
    {
        if (is_object($name)) {
            return $name;
        }

        if ( ! isset(self::$resolvedInstance[$name])) {
            self::$resolvedInstance[$name] = Hooks::make($name);
        }

        return self::$resolvedInstance[$name];
    }
}
