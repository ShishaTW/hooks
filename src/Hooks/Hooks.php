<?php

namespace Hooks;

class Hooks
{
    use Singleton, SingletonAccessor;
    
    protected static function getStatic()
    {
        return HookManager::class;
    }
}
