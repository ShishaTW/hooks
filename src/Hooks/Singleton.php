<?php

namespace Hooks;

use InvalidArgumentException;

trait Singleton
{
    private static $instance;
    
    /**
     * 取得 instance
     *
     * @return static
     */
    public static function getInstance()
    {
        $classname = static::getStatic();
        if (! static::$instance instanceof $classname) {
            return static::$instance = new $classname;
        }
        
        return static::$instance;
    }
    
    /**
     * 設置 instance
     *
     * @param static $instance
     * @return static
     * @throws \InvalidArgumentException
     */
    public static function setInstance($instance)
    {
        $classname = static::getStatic();
        if (! $instance instanceof $classname) {
            throw new InvalidArgumentException(
                'Argument 1 must be an instance of '. $classname
                . ', instance of ' . get_class($instance) . ' given.'
            );
        }
        
        return static::$instance = $instance;
    }
    
    /**
     * 取得實例名稱
     *
     * @return string
     */
    protected static function getStatic()
    {
        return static::class;
    }
}
