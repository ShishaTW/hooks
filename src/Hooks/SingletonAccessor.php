<?php

namespace Hooks;

trait SingletonAccessor
{
    /**
     * 動態呼叫 instance 方法
     *
     * @param  string $method
     * @param  array $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return static::getInstance()->$method(...$args);
    }
}
