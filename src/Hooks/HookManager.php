<?php

namespace Hooks;

use Illuminate\Config\Repository;
use Illuminate\Container\Container;

class HookManager
{
    /**
     * IoC 容器.
     *
     * @var \Illuminate\Container\Container
     */
    private $container;


    /**
     * 註冊的掛勾
     *
     * @var array
     */
    private $hooks = [];

    /**
     * 判斷是否已初始化
     *
     * @var boolean
     */
    private $booted = false;

    /**
     * 註冊一個服務提供者 (ServiceProvider).
     *
     * @param ServiceProvider|string $provider   被註冊的服務提供器名稱
     * @param string                 $configPath 設定檔路徑
     */
    public function addHook($provider, $configPath = null)
    {
        if (! $this->isBooted()) {
            $this->hooks[] = [$provider, $configPath];
            return;
        }

        $provider = $this->parseProviderClass($provider);
        $provider->register();
        $provider->boot();
    }

    /**
     * 處理 Service Provider.
     *
     * @param mixed $provider
     *
     * @return ServiceProvider
     */
    protected function parseProviderClass($provider)
    {
        if (is_string($provider)) {
            $provider = new $provider($this->container);
        }

        return $provider;
    }

    /**
     * 傳入 IoC 容器進行初始化.
     *
     * @param Illuminate\Container\Container $container IoC 容器
     */
    public function boot(Container $container = null)
    {
        if ($this->isBooted()) {
            return;
        }

        $this->container = $this->parseContainer($container);

        // 註冊服務提供者
        $providers = array_map(function ($hook) {
            list($provider, $configPath) = $hook;

            if ($configPath) {
                $this->mergeConfigFromPath($configPath);
            }

            $provider = $this->parseProviderClass($provider);
            $provider->register();

            return $provider;
        }, $this->hooks);

        // 初始化服務提供者
        array_walk($providers, function ($provider) {
            $provider->boot();
        });

        $this->booted = true;
    }

    /**
     * 處理 Container.
     *
     * @param mixed $container
     *
     * @return Illuminate\Container\Container
     */
    protected function parseContainer($container)
    {
        if (empty($container)) {
            $container = new Container();
            $container->instance('config', new Repository());
        }

        return $container;
    }

    /**
     * 載入並合併設定檔
     *
     * @param  string $path 載入至設定檔
     * @param  string $key  自訂 config 名稱
     *
     * @return void
     */
    public function mergeConfigFromPath($path, $key = null)
    {
        $key = $key ?: basename($path, '.php');

        $repository = $this->container['config'];
        $repository->set($key, array_merge($repository->get($key, []), require $path));
    }

    /**
     * 判斷是否已初始化.
     *
     * @return bool
     */
    public function isBooted()
    {
        return $this->booted;
    }

    /**
     * 生成已預先註冊於容器的類別實例。
     *
     * @param  string $abstract   在容器已註冊的名稱
     * @param  array $parameters  要傳入的參數
     *
     * @return mixed
     */
    public function make($abstract, array $parameters = [])
    {
        if (! $this->isBooted()) {
            $this->boot();
        }

        return $this->container->make($abstract, $parameters);
    }

    /**
     * 生成給定的類別實例。
     *
     * @param  mixed $concrete   要透過容器產生的類別
     * @param  array $parameters 要傳入的參數
     *
     * @return mixed
     */
    public function build($concrete, array $parameters = [])
    {
        if (! $this->isBooted()) {
            $this->boot();
        }

        return $this->container->build($concrete, $parameters);
    }
}
